package com.task14_json.parser;


import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import com.github.fge.jsonschema.util.JsonLoader;
import java.io.File;
import java.io.IOException;

public class JsonPlaneValidator {

  public static boolean validate(File json1, File json2) {
    try {
      JsonNode json = JsonLoader.fromFile(json1);
      JsonNode jsonSchema = JsonLoader.fromFile(json2);
      JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.byDefault();
      JsonValidator jsonValidator = jsonSchemaFactory.getValidator();
      System.out.println(jsonValidator.validate(jsonSchema, json));
      return true;
    } catch (IOException | ProcessingException e) {
      e.printStackTrace();
      return false;
    }

  }
}
