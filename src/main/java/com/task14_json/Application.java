package com.task14_json;

import com.task14_json.model.Plane;
import com.task14_json.parser.JsonPlaneParser;
import com.task14_json.parser.JsonPlaneValidator;
import java.io.File;
import java.util.List;

public class Application {


  public static void main(String[] args) {
    File file1 = new File("src/main/resources/plane.json");
    File file2 = new File("src/main/resources/plane1.json");

    List<Plane> planes = JsonPlaneParser.getObjectFromJson(file1);

    planes.forEach(System.out::println);

    planes.sort(new Plane());
    System.out.println("\nAfter sort\n");
    planes.forEach(System.out::println);

    System.out.println(JsonPlaneParser.setObjectToJson(planes.toArray()));

    JsonPlaneParser.getObjectFromJson(file2)
        .forEach(System.out::println);

    System.out.println(JsonPlaneValidator.validate(file1, file2));

  }
}
