package com.task14_json.model;

import java.util.Comparator;

public class Plane implements Comparator<Plane> {

  private String model;
  private String origin;
  private Chars chars;
  private Parameters parameters;
  private int price;

  public Plane() {
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public Chars getChars() {
    return chars;
  }

  public void setChars(Chars chars) {
    this.chars = chars;
  }

  public Parameters getParameters() {
    return parameters;
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return "Plane{" +
        "\n model='" + model + '\'' +
        ",\n origin='" + origin + '\'' +
        ",\n " + chars.toString() +
        ",\n " + parameters.toString() +
        ",\n price=" + price +
        '}';
  }


  @Override
  public int compare(Plane o1, Plane o2) {
    return Integer.compare(o1.getPrice(), o2.getPrice());
  }
}
