package com.task14_json.model;

public class Chars {

  private String type;
  private int numberOfSeats;
  private String combatKit;
  private boolean hasRadar;

  public Chars() {
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getNumberOfSeats() {
    return numberOfSeats;
  }

  public void setNumberOfSeats(int numberOfSeats) {
    this.numberOfSeats = numberOfSeats;
  }

  public String getCombatKit() {
    return combatKit;
  }

  public void setCombatKit(String combatKit) {
    this.combatKit = combatKit;
  }

  public boolean isHasRadar() {
    return hasRadar;
  }

  public void setHasRadar(boolean hasRadar) {
    this.hasRadar = hasRadar;
  }

  @Override
  public String toString() {
    return " Chars{" +
        "\n  type='" + type + '\'' +
        ",\n  numberOfSeats=" + numberOfSeats +
        ",\n  combatKit='" + combatKit + '\'' +
        ",\n  hasRadar=" + hasRadar +
        '}';
  }
}
